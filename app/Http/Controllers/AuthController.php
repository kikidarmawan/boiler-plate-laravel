<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function loginAction(Request $request)
    {
        $credentials = $request->only('username', 'password');

        $auth = auth()->attempt($credentials);
        if ($auth) {
            return redirect()->intended('dashboard');
        }

        return back()->with('error', 'username atau password salah.');
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('login');
    }
}
