<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            [
                'nama_lengkap' => 'Super Administrator',
                'username' => 'superadmin',
                'password' => bcrypt('superadmin'),
                'role_id' => 1,
            ],
            [
                'nama_lengkap' => 'Administrator',
                'username' => 'admin',
                'password' => bcrypt('admin'),
                'role_id' => 2,
            ],
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
